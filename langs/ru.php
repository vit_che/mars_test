<?php


return array(

    'MULTI_LANGUAGE' => "Многоязычный",
    'LOGIN' => "Вход",
    'USER_NAME' => "Имя пользователя",
    'ENTER_USER_NAME' => "Введите имя пользователя",
    'PASSWORD' => "Пароль",
    'NEW_PASSWORD' => "Новый пароль",
    'NOT_A_MEMBER' => "Ещё не зарегистрировались?",
    'REGISTRATION' => "Регистрация",
    'USERNAME_MUST' => "Имя пользователя должно быть длиной от 3 до 30 символов и содержать только латинские буквы",
    'EMAIL_ADDRESS' => "Электронный адрес",
    'ENTER_EMAIL' => 'Введите электронный адрес',
    'PASSWORD_MUST' => 'Пароль должен быть длиной от 8 до 30 символов, содержать латинские буквы, включать в себя не менее одной буквы верхнего регистра и не менее одной цифры',
    'ALREADY_REGISTERED' => 'Уже зарегистрированы',
    'REGISTER' => 'Регистрация',
    'PROFILE_PAGE' => 'Страница Профиля',
    'WELLCOME' => 'Добро пожаловать',
    'LOGOUT' => 'Выйти',
    'USER_PROFILE' => 'Профиль Пользователя',
    'EDIT_PROFILE' => 'Редактировать профиль',
    'USER_EMAIL' => 'Адрес пользователя',
    'SAVE' => 'Сохранить',
    'IMAGE_MUST' => 'Файл изображения должен иметь расширение gif, jpg, png или jpeg  и размер не более 128Kb',
    'PASSWORD_DOES_NOT' => "Пароль не соответствует требованиям",
    'NAME_CONTAINS' => "Имя содержит недопустимые символы",
    'EMAIL_NOT_CORRECT' => "Электронный адрес не корректный!",
    'WRONG_TYPE' => "Не правильный тип файла",
    'WRONG_SIZE' => "Не соответствующий размер файла",
    'EDIT_FORM' => "Форма редактирования пользователя",
    'IMAGE_INPUT' => "Загрузка файла изображения",
    'CURRENT_FILE' => "Существующий файл",
    'ALL_FIELDS_REQUIRED' => "Необходимо заполнить все поля",

    "USERNAME_EXIST" => "Пользователь с таким именем уже существует! Пожалуйста используйте другое имя!",
    "EMAIL_EXIST" => "Такой адрес уже сущесвует! пожалуйста используйте другой адрес!",
    "USER_NOT_REGISTERED" => "Такой пользователь не зарегистрирован!",
    "PASSWORD_NOT_CORRECT" => "Пароль не корректный!",
    "USER_CREATED" => "Пользователь создан",

    'NAME_IS_REQUIRED' => 'имя обязательно',
    'EMAIL_IS_REQUIRED' => 'электронный адрес обязателен',
    'PASSWORD_IS_REQUIRED' => 'требуется пароль',
    'ENTER_CORRECT_EMAIL' => 'Пожалуйста введите корректный адресс',
);
