TEST ACTIVITY: 

Please write using PHP, MySQL, JavaScript form
Login / register a new user. Think it Yourself
required fields. As a result of filling out the form, the user must
provide information about yourself. After logging in, the profile should be displayed
registered user. Using various PHP
no frameworks allowed.

1. The form must be made in a manner that is understandable to the user,
contain necessary instructions, comments, etc. (usability).
2. It should be possible to switch the language of the form interface to another
tongue.
3. The script should contain means of verification and field validation, and
also protection against incorrect data entry, special characters, hacking attempts
etc.
4. Validation and verification of fields should be carried out as on the client
side, and on the server side (using PHP).
5. The structure of the database should be reasonable.
6. In addition to entering text data, the user must register
be able to upload a graphic file formats gif, jpg, png.
7. Supporting texts in the form (field names, tips, errors and
etc.) must be formulated competently and clearly by the user,
sustained business and respectful style of treatment.
8. The code should be written clearly and accurately, with observance of tabulation
and other writing elements, without unnecessary elements and functions that do not have
relationship to the functionality of the test task, provided with clear
comments.
9. We draw attention to the fact that not only technical matters
part of the task (codes), but also design (appearance,
design logic, completeness of instructions).
10. Please, when completing the test task, pay special attention to
quality and security codes.

Particular attention should be paid to the following points:
- Readability and availability of elementary architecture, OOP is welcome
(the task where all the code in one file will be immediately rejected).
- The cleanliness and design of the code is no less important factor, the code should be
written in a single style (preferably recommended for a particular
language). Also, cleanliness includes the absence of copy paste and duplication
logic.
- Database optimization (indexing fields, etc.).
- Lack of obvious vulnerabilities (for example, SQL injection).
- Ability to work with JavaScript and its understanding.