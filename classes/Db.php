<?php

class Db
{
		public static function getConnection()
		{
			$configPath = ROOT . '/config/db_config.php';
			$params = include($configPath);
			$dsn = "mysql:host={$params['host']};dbname={$params['dbname']};port={$params['port']}";
			$db = new PDO($dsn, $params['user'], $params['password']);

			return $db;
		}
}
