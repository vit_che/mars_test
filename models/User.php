<?php


class User
{
    /**
     * return single user with specified id
     * @rapam integer &id
     */
    public static function getUserByID($id)
    {
        $id = intval($id);

        if ($id) {

            $db = Db::getConnection();
            $query = $db->prepare('SELECT * FROM users WHERE `id` = ?;');
            $query->execute(array($id));
            $query->setFetchMode(PDO::FETCH_ASSOC);
            $user = $query->fetch();

            return $user;
        }
    }

    /**
     * return User by Name
     */
    public static function getUserByName($name)
    {
        $db = Db::getConnection();
        $query = $db->prepare('SELECT * FROM users WHERE `name` = ?;');
        $query->execute(array($name));
        $query->setFetchMode(PDO::FETCH_ASSOC);
        $user = $query->fetch();

        return $user;
    }

    /**
     * add new User
     */
    public static function addUser($name, $email, $password)
    {
        $db = Db::getConnection();
        $password = md5($password);
        $insert_row = $db->prepare("INSERT INTO users ( name, password, email ) VALUES( :name, :password, :email )");
        $insert_row->execute(array('name'=>$name,'password'=>$password,'email'=>$email));

        return true;
    }

    /**
     * update User data
     */
    public static function updateUser($id, $name, $email, $password, $image_path, $file_name )
    {
        $db = Db::getConnection();
        $query = $db->prepare("UPDATE users SET `name`=:name, `password`=:password, `email`=:email, `image_path`=:image_path, `file_name`=:file_name WHERE `id` =:id");
        $query->execute(array('name'=>$name,'password'=>$password,'email'=>$email,'image_path'=>$image_path,'file_name'=>$file_name,'id'=>$id));

        return true;
    }

    /**
     * is exist User
     */
    public static function issetUser($name, $password)
    {
        $db = Db::getConnection();
        $password = md5($password);
        $query = $db->prepare('SELECT * FROM users WHERE `name` = ? and `password` = ?;');
        $query->execute(array($name, $password));
        $query->setFetchMode(PDO::FETCH_ASSOC);

        return ($query->fetch()) ? true : false;
    }
}
