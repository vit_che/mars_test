<?php

include_once ROOT. '/models/User.php';
require_once ROOT.'/vendor/autoload.php';
require_once ROOT.'/classes/Validation.php';
require_once ROOT.'/controllers/Controller.php';

class UsersController extends Controller
{
    /**
     * render User Profile Page
     */
    public function actionProfile($id)
    {

        if(isset($_SESSION["session_username"])){

            $loader = new \Twig\Loader\FilesystemLoader('views');
            $twig = new \Twig\Environment($loader);

            $template = $twig->load('profile.html');

            $user = User::getUserByName($_SESSION["session_username"]);

            $uid = $user['id'];

            if ($uid != $id) {

                $loader = new \Twig\Loader\FilesystemLoader('views');
                $twig = new \Twig\Environment($loader);

                $template = $twig->load('error.html');
                $error_message = "YOU try to get access to other user data";
                echo $template->render([ 'error_message' => $error_message]);

                exit();
            }
            
            $logout = "http://".$_SERVER['HTTP_HOST']."/".SITE."/login/out";
            $edit = "http://".$_SERVER['HTTP_HOST']."/".SITE."/edit";

            $messages = [];
            if(isset($_SESSION["session_messages"])){
                $messages = $_SESSION["session_messages"];
                unset($_SESSION['session_messages']);
            }

            $current_lang = self::getLangData();
            $lang = include(ROOT.'/langs/'.$current_lang['lang'].'.php');

            echo $template->render([
                'SITE' => SITE,
                'user' => $user,
                'logout' => $logout,
                'edit' => $edit,
                "messages" => $messages,
                'lang' => $lang,
                'current_lang' => $current_lang
            ]);

            return true;

        } else {

            header("Location: http://" . $_SERVER['HTTP_HOST']."/".SITE."/login");
            exit();
        }
    }


    /**
     * Create New User
     */
    public function actionCreate(){

        // language data
        $current_lang = self::getLangData()['lang'];
        $lang = include (ROOT.'/langs/'.$current_lang.'.php');

        if(isset($_POST) && count($_POST)>0 ) {

            if (!empty($_POST['name']) && !empty($_POST['password']) && !empty($_POST['email'])) {

                $name= Validation::clear_input($_POST['name']);
                $password= Validation::clear_input($_POST['password']);
                $email= Validation::clear_input($_POST['email']);

                $messages = [];

                $validateNameResult = Validation::validateName($name, $lang);
                if (isset($validateNameResult) && count($validateNameResult) > 0){
                    $messages = $validateNameResult;
                }
                $validateEmailResult = Validation::validateEmail($email, $lang);
                if (isset($validateEmailResult) && count($validateEmailResult) > 0){
                    $messages[] = $validateEmailResult;
                }
                $validatePasswordResult = Validation::validatePassword($password, $lang);
                if (isset($validatePasswordResult) && count($validatePasswordResult) > 0){
                    $messages[] = $validatePasswordResult;
                }
                $checkUserNameResult = Validation::checkUserName($name, $lang);
                if ( !$checkUserNameResult['result']){
                    $messages[] = $checkUserNameResult;
                }
                $checkUserEmailResult = Validation::checkUserEmail($email, $lang);
                if ( !$checkUserEmailResult['result']){
                    $messages[] = $checkUserEmailResult;
                }

                if (count($messages) == 0) {

                    if (User::addUser($name, $email, $password)) {

                        $messages[] = [
                            "status" => "success",
                            "message" => $lang['USER_CREATED']
                        ];

                        $_SESSION['session_messages'] = $messages;
                        header("Location: http://" . $_SERVER['HTTP_HOST']."/".SITE."/login");
                        exit();
                    }

                } else {

                    $_SESSION['session_messages'] = $messages;
                    header("Location: http://" . $_SERVER['HTTP_HOST']."/".SITE. "/register");
                    exit();
                }

            } else {

                $messages[] = [
                    "status" => "warning",
                    "message" => $lang['ALL_FIELDS_REQUIRED']
                ];

                $_SESSION['session_messages'] = $messages;
                header("Location: http://" . $_SERVER['HTTP_HOST']."/".SITE. "/register");
                exit();
            }
        }
    }


    /**
     * render User Register Form
     */
    public function actionRegister(){

        if(isset($_SESSION["session_username"])){
            $user = User::getUserByName($_SESSION["session_username"]);
            if($user){
                header("Location: http://".$_SERVER['HTTP_HOST']."/".SITE."/users/".$user['id']);
                exit();
            }
        }

        $loader = new \Twig\Loader\FilesystemLoader('views');
        $twig = new \Twig\Environment($loader);
        $template = $twig->load('register.html');

        $messages = NULL;
        if(isset($_SESSION["session_messages"])){
            $messages = $_SESSION['session_messages'];
            unset($_SESSION['session_messages']);
        }

        $current_lang = self::getLangData();
        $lang = include(ROOT.'/langs/'.$current_lang['lang'].'.php');

        echo $template->render(['messages' => $messages, 'lang' => $lang, "current_lang" => $current_lang]);

        return true;
    }


    /**
     * render User Edit Form
     */
    public function actionEdit(){

        $loader = new \Twig\Loader\FilesystemLoader('views');
        $twig = new \Twig\Environment($loader);
        $template = $twig->load('edit.html');

        $user = User::getUserByName($_SESSION["session_username"]);
        $profile = "http://".$_SERVER['HTTP_HOST']."/".SITE."/users/".$user['id'];

        $messages = [];

        if(isset($_SESSION["session_messages"])){
            $messages = $_SESSION['session_messages'];
            unset($_SESSION['session_messages']);
        }

        $current_lang = self::getLangData();
        $lang = include(ROOT.'/langs/'.$current_lang['lang'].'.php');


        echo $template->render(['messages' => $messages, 'user' => $user, 'profile' => $profile, 'lang' => $lang, 'current_lang' => $current_lang]);

        return true;
    }

    /**
     * render Update User Data
     */
    public function actionUpdate(){

        // language data
        $current_lang = self::getLangData()['lang'];
        $lang = include (ROOT.'/langs/'.$current_lang.'.php');

        $user = User::getUserByName($_SESSION["session_username"]);
        $messages = [];

        if(isset($_POST["form"])){

            if(!empty($_POST['name']) && !empty($_POST['email'])) {

                $name= Validation::clear_input($_POST['name']);
                $email= Validation::clear_input($_POST['email']);
                $error_messages = [];

                $validateNameResult = Validation::validateName($name, $lang);
                if (isset($validateNameResult) && count($validateNameResult) > 0){
                    $error_messages = $validateNameResult;
                }

                $validateEmailResult = Validation::validateEmail($email, $lang);
                if (isset($validateEmailResult) && count($validateEmailResult) > 0){
                    $error_messages[] = $validateEmailResult;
                }

                if ($_POST['new_password']){

                    $validatePasswordResult = Validation::validatePassword($_POST['new_password'], $lang);
                    if (isset($validatePasswordResult) && count($validatePasswordResult) > 0){
                        $error_messages[] = $validatePasswordResult;
                    } else {
                        $password= md5(Validation::clear_input($_POST['new_password']));
                    }
                } else {
                    $password = $user['password'];
                }

                $old_name = $user['name'];
                $checkUserNameResult = Validation::checkEditUserName($name, $old_name);
                if ( !$checkUserNameResult['result']){
                    $error_messages[] = $checkUserNameResult;
                }

                $old_email = $user['email'];
                $checkUserEmailResult = Validation::checkEditUserEmail($email, $old_email, $lang);
                if ( !$checkUserEmailResult['result']){
                    $error_messages[] = $checkUserEmailResult;
                }

                if ($_FILES['image']['tmp_name']){

                    $validateImageResult = Validation::validateImage($_FILES, $lang);
                    if (count($validateImageResult) == 0) {
                        $file_name = $user['name'].'_'.$_FILES['image']['name'];
                        $path = '/images/'.$file_name;
                    } else {
                        $error_messages = array_merge($error_messages, $validateImageResult);
                    }

                } else {
                    $path = $user['image_path'];
                    $file_name = $user['file_name'];
                }

                if (count($error_messages) == 0) {

                    $upload = ROOT.$path;
                    move_uploaded_file($_FILES['image']['tmp_name'], $upload);

                    if ( $path != $user['image_path']  &&   $user['image_path'] != '/images/default.png'){
                        $old_path = ROOT.$user['image_path'];
                        if (file_exists($old_path)){
                            unlink($old_path);
                        }
                    }

                    $id = intval($user['id']);
                    if(User::updateUser($id, $name, $email, $password, $path, $file_name)){
                        $messages[] = [
                            "status" => "success",
                            "message" => "User Profile was updated successfully"
                        ];
                        $_SESSION['session_messages'] = $messages;
                    }

                    $_SESSION['session_username']=$name;
                    header("Location: http://".$_SERVER['HTTP_HOST']."/".SITE."/users/".$id);
                    exit();

                } else {
                    $messages = $error_messages;

                    $_SESSION['session_messages'] = $messages;
                    header("Location: http://" . $_SERVER['HTTP_HOST']."/".SITE."/edit");
                    exit();
                }

            } else {
                $messages[] = [
                    "status" => "warning",
                    "message" => $lang['ALL_FIELDS_REQUIRED']
                ];

                $_SESSION['session_messages'] = $messages;
                header("Location: http://" . $_SERVER['HTTP_HOST']."/".SITE."/edit");
                exit();
            }
        }
    }
}
