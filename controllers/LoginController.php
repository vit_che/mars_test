<?php

include_once (ROOT. '/models/User.php');
require_once (ROOT.'/vendor/autoload.php');
require_once (ROOT.'/controllers/UsersController.php');
require_once (ROOT.'/controllers/Controller.php');


class LoginController extends Controller
{
    /**
     * render Login Page
     */
    public function actionIndex(){

//        var_dump($_SERVER['REMOTE_ADDR']);

        $messages = NULL;

        if(isset($_SESSION["session_username"])){
            $user = User::getUserByName($_SESSION["session_username"]);
            if($user){
                header("Location: http://".$_SERVER['HTTP_HOST']."/".SITE."/users/".$user['id']);
                exit();
            }
        }

        if(isset($_SESSION["session_messages"])){
            $messages = $_SESSION['session_messages'];
            unset($_SESSION['session_messages']);
        }

        $loader = new \Twig\Loader\FilesystemLoader('views');
        $twig = new \Twig\Environment($loader);
        $template = $twig->load('login.html');

        $current_lang = self::getLangData();
        $lang = include(ROOT.'/langs/'.$current_lang['lang'].'.php');

        echo $template->render([ 'messages' => $messages, 'lang' => $lang, 'current_lang' => $current_lang]);

        return true;
    }

    /**
     * enter User
     */
    public function actionEnter(){

        $current_lang = self::getLangData();
        $lang = include(ROOT.'/langs/'.$current_lang['lang'].'.php');

        if(isset($_POST) && count($_POST)>0 ) {

            if (!empty($_POST['name']) && !empty($_POST['password'])) {

                $name= Validation::clear_input($_POST['name']);
                $password= Validation::clear_input($_POST['password']);

                $check_creds_result = Validation::checkUserNameAndPassword($name, $password, $lang);

                if ( $check_creds_result['result']){

                    $_SESSION['session_username'] = $name;
                    $user = User::getUserByName($name);
                    header("Location: http://".$_SERVER['HTTP_HOST']."/".SITE."/users/".$user['id']);
                    exit();

                } else {
                    $_SESSION['session_messages'] =  $check_creds_result['messages'];
                    header("Location: http://".$_SERVER['HTTP_HOST']."/".SITE."/login");
                    exit();
                }

            } else {

                $messages[] = [
                    "status" => "warning",
                    "message" => $lang['ALL_FIELDS_REQUIRED']
                ];

                $_SESSION['session_messages'] = $messages;
                header("Location: http://" . $_SERVER['HTTP_HOST']."/".SITE."/login");
                exit();
            }
        }
    }


    /**
     * out User
     */
    public function actionOut(){

        unset($_SESSION['session_username']);
        session_destroy();
        header("Location: http://".$_SERVER['HTTP_HOST']."/".SITE."/login");
        exit();
    }
}
