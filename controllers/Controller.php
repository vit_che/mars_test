<?php

class Controller
{
    public static function getLangData()
    {
        if (isset($_SESSION["lang"])) {

            $lang = $_SESSION['lang'];

        } else {

            $lang = 'en';
        }

        $uri = $_SERVER["REQUEST_URI"];

        if (strripos($uri,'?')){
            $uri = mb_stristr($uri, '?', true);
        }

        $lang_data = [
            'lang' => $lang,
            'link_ru' => $uri."?lang=ru",
            'link_en' => $uri."?lang=en",
        ];

        return $lang_data;
    }
}
