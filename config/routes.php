<?php
return array(

	'users/([0-9]+)' => 'users/profile/$1',
	'create' => 'users/create',
	'register' => 'users/register',
	'edit' => 'users/edit',
	'update' => 'users/update',
	'login/enter' => 'login/enter',
	'login/out' => 'login/out',
    'login' => 'login/index',
    '' => 'login/index',
	);
